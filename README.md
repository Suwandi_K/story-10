# Project Status
| Pipeline | Coverage |
| ------ | ------ |
|[![pipeline status](https://gitlab.com/Suwandi_K/story-10/badges/master/pipeline.svg)](https://gitlab.com/Suwandi_K/story-10/commits/master)| [![coverage report](https://gitlab.com/Suwandi_K/story-10/badges/master/coverage.svg)](https://gitlab.com/Suwandi_K/story-10/commits/master) |

# Story 10 PPW
Story 10 PPW Fasilkom UI Tahun Pelajaran 2019/2020 Semester Genap

# Data diri
<ol>
    <li>Nama    : Suwandi Kurniawan</li>
    <li>NPM     : 1906299175</li>
    <li>Kelas   : PPW</li>
    <li>Deployed at : https://story10-suwandi.herokuapp.com/</li>
</ol>