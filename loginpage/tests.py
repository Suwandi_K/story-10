from django.test import TestCase , Client, LiveServerTestCase
from django.http import HttpRequest
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait

import os
import time

# Create your tests here.
class LoginpageUnitTest(TestCase):
    def test_redirect_to_login_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)
    
    def test_bad_request_api(self):
        response = Client().get('/api/v1/login/')
        self.assertEqual(response.status_code, 400)

        response = Client().get('/api/v1/signup/')
        self.assertEqual(response.status_code, 400)

class LoginpageFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(LoginpageFunctionalTest, self).setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)        

    def tearDown(self):
        self.browser.quit()

        super(LoginpageFunctionalTest, self).tearDown()

    def test_login_in_loginpage(self):
        User.objects.create_user('hello', 'hello@gmail.com', 'hello123')

        self.browser.get(self.live_server_url + '/')
 
        time.sleep(5)

        username = self.browser.find_element_by_name("username")
        username.send_keys("hello")

        password = self.browser.find_element_by_name("password")
        password.send_keys("hello123")

        login_button = self.browser.find_element_by_xpath("//button[contains(.,'Log In')][1]")
        login_button.click()

        time.sleep(5)
        self.assertIn('hello', self.browser.page_source) 

        logout_button = self.browser.find_element_by_xpath('//button[contains(.,"Logout")]')
        logout_button.click()

        time.sleep(5)
        self.assertNotIn('hello', self.browser.page_source) 

    def test_login_page_user_does_not_exist(self):
        self.browser.get(self.live_server_url + '/')
 
        time.sleep(5)

        username = self.browser.find_element_by_name("username")
        username.send_keys("HALO")

        password = self.browser.find_element_by_name("password")
        password.send_keys("kmbui2019")

        login_button = self.browser.find_element_by_xpath("//button[contains(.,'Log In')][1]")
        login_button.click()

        time.sleep(5)

        self.assertIn('Username tidak terdaftar, mohon periksa kembali dan coba lagi', self.browser.page_source) 

    def test_login_page_wrong_password(self):
        User.objects.create_user('Wandi', 'hello@yahoo.com', 'hellogais')

        self.browser.get(self.live_server_url + '/')
 
        time.sleep(5)

        username = self.browser.find_element_by_name("username")
        username.send_keys("Wandi")

        password = self.browser.find_element_by_name("password")
        password.send_keys("salahgais")

        login_button = self.browser.find_element_by_xpath("//button[contains(.,'Log In')][1]")
        login_button.click()

        time.sleep(5)

        self.assertIn('Username / password salah, mohon periksa kembali dan coba lagi', self.browser.page_source)

    def test_create_new_account_and_try_duplicate_username(self):
        self.browser.get(self.live_server_url + '/')

        time.sleep(5)

        sign_up_button = self.browser.find_element_by_xpath('//a[contains(.,"Create an account")]')
        sign_up_button.click()

        time.sleep(5)

        username = self.browser.find_element_by_name("s_username")
        username.send_keys("Test")

        email = self.browser.find_element_by_name("s_email")
        email.send_keys("test@gmail.com")

        password = self.browser.find_element_by_name("s_password")
        password.send_keys("keepsmile89")

        sign_up_button = self.browser.find_element_by_xpath('//button[contains(.,"Sign Up")]')
        sign_up_button.click()

        time.sleep(5)

        user = authenticate(username="Test", password="keepsmile89")
        self.assertTrue(user) 

        sign_up_button = self.browser.find_element_by_xpath('//a[contains(.,"Create an account")]')
        sign_up_button.click()

        time.sleep(5)

        username = self.browser.find_element_by_name("s_username")
        username.clear()
        username.send_keys("Test")

        email = self.browser.find_element_by_name("s_email")
        email.clear()
        email.send_keys("test@gmail.com")

        password = self.browser.find_element_by_name("s_password")
        password.clear()
        password.send_keys("semangat45")

        sign_up_button = self.browser.find_element_by_xpath('//button[contains(.,"Sign Up")]')
        sign_up_button.click()

        time.sleep(5)

        self.assertIn('Username sudah dipakai, coba ganti dengan username lain', self.browser.page_source)
